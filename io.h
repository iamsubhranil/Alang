#ifndef IO_H
#define IO_H

#include "values.h"

Data getString();
Data getInt();
Data getFloat();

#endif
